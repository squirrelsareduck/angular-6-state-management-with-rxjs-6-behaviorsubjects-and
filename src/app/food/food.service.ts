import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Food} from './food.model';
import {BehaviorSubject, Observable, Subject} from 'rxjs';

const initialFood: Food = {
  flavors: ['Salty', 'Bread', 'Salty', 'Bread', 'Salty', 'Bread'],
  meats: ['ham', 'salami', 'Salty', 'Bread', 'Salty', 'Bread', 'Bread'],
  randomOtherStuff: 'I am cool.',
  preferences: 'I prefer to be myself.',
};

@Injectable({
  providedIn: 'root'
})
export class FoodService {
  private _food$: BehaviorSubject<Food> = new BehaviorSubject<Food>(initialFood);
  // private _food$: Subject<Food> = new Subject<Food>();
  public food$: Observable<Food> = this._food$.asObservable();
  constructor(private httpClient: HttpClient) {
    window.setInterval(() => {
      this.httpClient.get('https://baconipsum.com/api/?type=all-meat', {responseType: 'text'})
        .subscribe((response: string) => {
         const data: string[] = response.replace('[', '').replace(']', '')
           .split(' ');
         const food: Food = {
           flavors: data.slice(0, 4),
           meats: data.slice(6, 12),
           randomOtherStuff: 'I am cool.',
           preferences: 'I prefer to be myself.',
         };
          this._food$.next(food);
        });
    }, 6500);
  }
}
