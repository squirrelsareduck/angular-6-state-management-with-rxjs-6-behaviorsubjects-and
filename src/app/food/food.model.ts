export interface Food {
  flavors: string[];
  meats: string[];
  randomOtherStuff: string;
  preferences: string;
}
