import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {FoodService} from '../food.service';
import {Observable} from 'rxjs';
import {Food} from '../food.model';
import {map} from 'rxjs/operators';

@Component({
  selector: 'app-awesome',
  templateUrl: './awesome.component.html',
  styleUrls: ['./awesome.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AwesomeComponent {

  food$: Observable<Food>;
  awesome$: Observable<string>;
  constructor(private foodService: FoodService) {
    this.food$ = foodService.food$;
    this.awesome$ = this.food$.pipe(map((food: Food) => {
     return `Awesome!!!! ${food.flavors[0]} ${food.flavors[1]} ${food.flavors[3]}`;
     }));
  }

}
