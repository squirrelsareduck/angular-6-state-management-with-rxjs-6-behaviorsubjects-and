import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {FoodService} from '../food.service';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Food} from '../food.model';

@Component({
  selector: 'app-great',
  templateUrl: './great.component.html',
  styleUrls: ['./great.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GreatComponent {
  great$: Observable<string>;
  constructor(private foodService: FoodService) {
    this.great$ = this.foodService.food$.pipe(map((food: Food) => {
      return `Great!!!! ${food.meats[5]} ${food.meats[1]} ${food.meats[3]}`;
    }));
  }

}
