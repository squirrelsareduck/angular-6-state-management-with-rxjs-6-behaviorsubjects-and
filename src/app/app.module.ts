import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { GreatComponent } from './food/great/great.component';
import { AwesomeComponent } from './food/awesome/awesome.component';
import {HttpClientModule} from '@angular/common/http';
import {FoodService} from './food/food.service';
import {RouterModule, Routes} from '@angular/router';

const appRoutes: Routes = [
  { path: 'first', component: AwesomeComponent },
  { path: 'second', component: GreatComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    GreatComponent,
    AwesomeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [FoodService],
  bootstrap: [AppComponent]
})
export class AppModule { }
